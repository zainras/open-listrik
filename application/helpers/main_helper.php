<?php

function msg($alert,$content)
{
	$ret = '<div class="alert alert-'.$alert.' alert-dismissable">
  				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  				'.$content.'
			</div>';

	return $ret;
}

function rupiah($number)
{
	$nominal = number_format($number, 0, '', '.');
	$ret = 'Rp '.$nominal;
	return $ret;
}
// default return awal
// plus1 = add one month
function tahunBulan($tahun,$bulan,$ret='awal')
{
	$tgl		= strtotime($tahun.'-'.$bulan);
	$plusMonth	= strtotime('+1 months',$tgl);
	$awal 		= date('Y-m-d H:i:s',$tgl);
	$akhir 		= date('Y-m-d H:i:s',$plusMonth);
	if ($ret == 'akhir')
	{
		return $akhir;
	}else{
		return $awal;
	}
}

function optionSelected($value,$fromDB)
{
	return ($value == $fromDB) ? 'selected' : '';
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtransaksi extends CI_Model {

	public function addTransaksi($data)
	{
		$data = [
			'name'			=>$data['name'],
			'nominal'		=>$data['nominal'],
			'type'			=>$data['type'],
			'description'	=>(isset($data['description'])) ? $data['description'] : '',
			'modified'		=>date('Y-m-d H:i:s'),
			'isdeleted'		=>'0',
		];
		$this->db->insert('transaksi', $data);
		$id = $this->db->insert_id();
		$this->_setHistoryTransaksi($id,$data);
	}

	public function getTransaksi($idTransaksi)
	{
		$this->db->where('id_transaksi', $idTransaksi);
		$q = $this->db->get('transaksi');
		$r = $q->row();
		return $r;
	}

	public function getHistoryTransaksi($idTransaksi) 
	{
		$this->db->select('trs.*');
		// $this->db->select('DISTINCT(trs.id');
		$this->db->select('trs_history.*');
		$this->db->from('transaksi as trs');
		$this->db->join('transaksi_history as trs_history','trs.id_transaksi=trs_history.id_transaksi','left');
		$this->db->where('trs.id_transaksi', $idTransaksi);
		$this->db->order_by('trs_history.created','desc');
		$q = $this->db->get();
		$r = $q->result();
		$result = new stdClass();
		foreach ($r as $key => $value) {
			// var_dump($key);die;
			$history[] = [
				'logs' => json_decode($value->logs),
				'created' => $value->created,
			];
			$result->id_transaksi = $value->id_transaksi;
			$result->name = $value->name;
			$result->nominal = $value->nominal;
			$result->type = $value->type;
			$result->description = $value->description;
			$result->isdeleted = $value->isdeleted;
			$result->history = $history;
		}
		return $result;
	}
	public function editTransaksi($idTransaksi,$data,$oldTransaksi)
	{
		$data['modified'] = date('Y-m-d H:i:s');
		$this->db->where('id_transaksi', $idTransaksi);
		$this->db->update('transaksi', $data);
		$this->_setHistoryTransaksi($idTransaksi,$oldTransaksi);

	}

	private function _setHistoryTransaksi($idTransaksi,$data)
	{
		$history = [
			'id_transaksi' 	=> $idTransaksi,
			'logs' 			=> json_encode($data),
			'created'		=> date('Y-m-d H:i:s'),
		];
		$this->db->insert('transaksi_history', $history);		
	}

}

/* End of file Mtransaksi.php */
/* Location: ./application/models/Mtransaksi.php */
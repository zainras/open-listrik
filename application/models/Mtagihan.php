<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtagihan extends CI_Model {

	public function getPelanggan($id)
	{
		$this->db->select('*');
		$this->db->from('pelanggan');
		$this->db->where('no_rekening', $id);
		$q = $this->db->get();
		$r = $q->row();
		return $r;
	}

	public function getAll()
	{
		$q = $this->db->get('tagihan');
		return $q->result();
	}

	public function getTagihan($filter=null)
	{
		$filter = '2017-05-03';
		$this->db->select('plg.nama_lengkap as nama');
		$this->db->select('ROUND(tgh.nominal,-3)+2000 as nominal');
		$this->db->select('DATE_FORMAT(tgh.periode, "%M-%Y") as periode');
		$this->db->from('tagihan as tgh');
		$this->db->join('pelanggan as plg', 'tgh.no_rekening = plg.no_rekening', 'left');
		if (!empty($filter))
		{
			// $this->db->where('tgh.periode <=', $filter);
			// $this->db->where('tgh.periode >=', $filter);
		}
		$q = $this->db->get();
		$r = $q->result();
		return $r;
	}

	public function get($filter=null)
	{
		// $this->db->select('plg.nama_lengkap as nama');
		$this->db->select_sum('nominal');
		// $this->db->select('ROUND(tgh.nominal,-3)+2000 as nominal');
		// $this->db->select('periode');
		$this->db->select('DATE_FORMAT(tgh.periode, "%m-%Y") as periode');
		$this->db->from('tagihan as tgh');
		$this->db->join('pelanggan as plg', 'tgh.no_rekening = plg.no_rekening', 'left');
		$this->db->group_by('MONTH(periode)');
		$q = $this->db->get();
		$r = $q->result();
		return $r;
	}



	public function getListPelanggan($value=null,$key=null,$filterStatus=false)
	{
		$this->db->select('*');
		$this->db->from('pelanggan');
		if ($key !=null && $value != null)
		{
			$this->db->where($key, $value);
		}
		if ($filterStatus==true)
		{
			$this->db->where('status', 1);
			$this->db->where('isdelete', 0);
		}
		$q = $this->db->get();
		$r = $q->result();
		return $r;
	}

	public function addPelanggan()
	{
		$ins = [
			'no_rekening' 	=> $this->input->post('no-rekening-pelanggan',true),
			'nama_lengkap' 	=> $this->input->post('nama-pelanggan',true),
			'atas_nama' 	=> $this->input->post('atas-nama-pelanggan',true),
			'alamat' 		=> $this->input->post('alamat-pelanggan',true),
			'nomor_hp'		=> $this->input->post('nohp-pelanggan',true),
			'created' 		=> date('Y-m-d H:i:s'),
			'modified' 		=> date('Y-m-d H:i:s'),
			'status' 		=> 1,
		];
		$this->db->insert('pelanggan', $ins);
	}

	public function editPelanggan($id)
	{
		$update = [
			'no_rekening' 	=> $this->input->post('no-rekening-pelanggan',true),
			'nama_lengkap' 	=> $this->input->post('nama-pelanggan',true),
			'atas_nama' 	=> $this->input->post('atas-nama-pelanggan',true),
			'alamat' 		=> $this->input->post('alamat-pelanggan',true),
			'nomor_hp'		=> $this->input->post('nohp-pelanggan',true),
			'created' 		=> date('Y-m-d H:i:s'),
			'modified' 		=> date('Y-m-d H:i:s'),
			'status' 		=> 1,
		];
		$this->db->where('no_rekening', $id);
		$this->db->update('pelanggan', $update);
	}

	public function editStatusTagihan()
	{
		$id 	=  $this->input->get('id',true);
		$status =  $this->input->get('id_status',true);
		if (isset($id) && isset($status))
		{
			$this->db->trans_begin();

			$status = ($status == 0) ? 1 : 0 ;
			$update['status'] 	= $status;
			$update['modified']	= date('Y-m-d H:i:s');
			$this->db->where('id', $id);
			$this->db->update('tagihan', $update);

			if ($this->db->trans_status() === FALSE)
			{
				$data['msg'] = 'Update Gagal';
				$this->db->trans_rollback();
			}
			else
			{
				$data['id']	= $id;
				$data['msg'] = 'Update Berhasil';
				$this->db->trans_commit();
			}
		
		} else {
			$data['msg'] = 'Data Error';
		}
		return $data;
	}

	public function cancelDeletePelanggan($id)
	{
		$cancel['isdelete'] = 0;
		$this->db->where('no_rekening', $id);
		$this->db->update('pelanggan', $cancel);
	}

	public function cekId($id)
	{
		$ret = false;
		$this->db->where('no_rekening', $id);
		$cek = $this->db->count_all_results('pelanggan');
		if ($cek > 0) {
			$ret = true;
		}
		return $ret;
	}

	public function getTagihanNow()
	{
		$this->db->select('DATE_FORMAT(t.periode, "%M %Y") as tgl');
		$this->db->select_sum('ROUND(t.nominal,-3)','total');
		$this->db->select_sum('IF(t.status = "1", ROUND(t.nominal,-3) , NULL)', 'terkumpul');
		$this->db->from('tagihan as t');
		$this->db->join('pelanggan as p', 't.no_rekening = p.no_rekening');
		$this->db->where('p.status',1);
		$this->db->where('p.isdelete',0);
		if ($this->input->get('filter'))
		{
			$tahun = $this->input->get('tahun',true);
			$bulan = $this->input->get('bulan',true);
			$this->db->where('t.periode >=', tahunBulan($tahun,$bulan,'awal'));
			$this->db->where('t.periode <', tahunBulan($tahun,$bulan,'akhir'));
		}
		$subquery = $this->db->get_compiled_select();
		$this->db->select('tgl');
		$this->db->select('total');
		$this->db->select('terkumpul');
		$this->db->select('(`total` - `terkumpul`) as `kurang_bayar`');
		$this->db->from('('.$subquery.') as subs' );
		$q = $this->db->get();
		$r = $q->row();
		return $r;

	}

	public function newData()
	{
		$ret = '';
		$ins = [];
		$bulan 		= '7';
		$tahun 		= '2017';
		$tgl		= strtotime($tahun.'-'.$bulan);
		$plusMonth	= strtotime('+1 months',$tgl);
		$awal 		= date('Y-m-d H:i:s',$tgl);
		$akhir 		= date('Y-m-d H:i:s',$plusMonth);
		$this->db->where('periode >=', $awal);
		$this->db->where('periode <', $akhir);
		$q = $this->db->get('tagihan');
		$data = $this->getListPelanggan(null,null);
		if ($q->num_rows() <= 0  ) {
			foreach ($data as $key => $value) {
				$ins[] = [
					'no_rekening' 	=> $value->no_rekening,
					'periode'		=> $awal,
					'modified'		=> $awal,	
				];
			}
			$this->db->insert_batch('tagihan', $ins);
			$ret = 'Data tagihan Baru berhasil ditambahkan';
		} else {
			$ret = 'Gagal Menambahkan Data tagihan';
		}
		var_dump($ret);die;
		return $ret;
	}

	public function getDatatable($list=false,$filter=false,$defaultOrder=false,$action=false)
	{

		if (!$list)
		{
			$list = [
						['select' => 'p.no_rekening','as' => 'no'],
						['select' => 'p.nama_lengkap','as' => 'nama'],
						['select' => 'p.atas_nama','as' => 'an'],
						['select' => 'ROUND(t.nominal,-3)','as' => 'nominal'],
						['select' => 'DATE_FORMAT(t.periode, "%M %Y")','as' => 'tgl'],
						['select' => 't.status','as' => 'status'],
					];
		}
		if (!$filter)
		{
			$filter = [
				['where'=>'p.isdelete','key'=>'0'],
				['where'=>'p.status','key'=>'1'],
			];
		}
		if (!$defaultOrder)
		{
			$defaultOrder = ['t.modified','desc'];
		}

		$action = true;

		$start = $this->input->post('start');
		$limit = $this->input->post('length');
		if($start !=null && $limit !=null){
			$this->db->limit($limit,$start);
		}

		$search = $this->input->post('search');
		if ($search['value'] !=''){
			$key = $search['value']; // data transaksi not deleted
			// $key 	= $this->db->escape_like_str($search['value']);
			$this->db->group_start();
			foreach ($list as $ls) {
				$this->db->or_like($ls['select'], $key);
			}			
			$this->db->group_end();
		}
		$order = $this->input->post('order');
		$column = isset($order[0]['column'])?$order[0]['column']:-1;
		if($column >= 0 && $column < count($list)){
			$ord = $list[$column]['as'];
			$by = $order[0]['dir'];
			$this->db->order_by($ord , $by);
		} else {
			$ord 	= $defaultOrder[0];
			$by 	= $defaultOrder[1];
			$this->db->order_by($ord , $by);
		}

		$this->db->select("SQL_CALC_FOUND_ROWS ".$list[0]['select'] ,FALSE);
		$this->db->select('t.id');
		//dynamic select
		foreach($list as $key => $ls) { 
			$this->db->select($ls['select'].' as '.$ls['as']);
		}
		// ex : $this->db->select('transaksi.name');

		$this->db->from('pelanggan as p');
		$this->db->join('tagihan as t', 'p.no_rekening = t.no_rekening', 'left');

		//dynamic filter
		foreach($filter as $key => $fil) {
			$this->db->where($fil['where'],$fil['key']);
		}
		
		$sql = $this->db->get();
		$q = $sql->result_array();
		// #1 main query
		$this->db->select("FOUND_ROWS() AS total_row");
		
		// #2 row (FOUND_ROWS)
		$row = $this->db->get()->row();

		$sEcho = $this->input->post('draw');

		// #3 count_all
		$getCountAll = $this->db->count_all_results('pelanggan');
		
		$output = array(
			"draw" => intval($sEcho),
			"recordsTotal" => $getCountAll,
			"recordsFiltered" => $row->total_row,
			"data" => array()
		);

		foreach ($q as $val) {

			if ($val[$list[5]['as']] == 1)
			{
				$act = '<a href="/tagihan/edit?id='.$val['id'].'&id_status='.$val['status'].'" class="btn btn-xs btn-datatable btn-danger">Batalkan</a>';
			} else {
				$act = '<a href="/tagihan/edit?id='.$val['id'].'&id_status='.$val['status'].'" class="btn btn-xs btn-datatable btn-primary">Bayar</a>';
			}
		
		$status = ($val[$list[5]['as']] == 1) ? '<a class="">lunas</a>' : '<a class="">belum bayar</a>';
		$output['data'][] = [
			$val[$list[0]['as']],
			$val[$list[1]['as']],
			$val[$list[2]['as']],
			$val[$list[3]['as']],
			$val[$list[4]['as']],
			$status,
			$act,
		];
		
		}
		return $output;
	}

	public function getQuerySetting()
	{

	}

}

/* End of file Mpelanggan.php */
/* Location: ./application/models/Mpelanggan.php */
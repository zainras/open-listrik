<section class="section-body">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Dashboard</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bar-chart-o fa-fw"></i> Area Chart Example
                        <div class="pull-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                    Actions
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="#">Save to Offline Mode</a>
                                    </li>
                                    <li><a href="#">Offline Mode</a>
                                    </li>
                                    <li><a href="#">Reload</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <form id="filter-datatable" method="get">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                    <?=set_select('name','bulan')?>
                                        <select id="tagihan-now-tahun" name="tahun" class="form-control">
                                            <option value="">Pilih Tahun</option>
                                            <?php for ($i='2017'; $i <= date('Y')+1 ; $i++) { ?>
                                            <option value="<?=$i?>" <?=($i == $tahun) ? 'selected' : '' ?>><?=$i?></option>
                                            <?php } ?> 
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <select id="tagihan-now-bulan" name="bulan" class="form-control">
                                            <option value="">Pilih Bulan</option>
                                            <?php for ($i='1'; $i <= '12' ; $i++) { ?>
                                            <option value="<?=$i?>" <?=($i == $bulan) ? 'selected' : '' ?>><?=$i?></option>
                                            <?php } ?> 
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <button type="submit" class="btn" id="filter-submit" name="filter" value="tagihan">GO</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <table class="datatable table table-responsive table-bordered table-striped table-hover" data-url="api/main/datatable<?=isset($filter)?$filter:''?>" data-type='tagihan-mobile'>
                            <thead>
                                <!-- <th>NO Rekening</th> -->
                                <th></th>
                                <th>Nama Lengkap</th>
                                <th>Nominal</th>
                                <!-- <th>Status</th> -->
                                <th>action</th>
                            </thead>
                        </table>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
            <!-- /.col-lg-8 -->
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div id="tagihan-now">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Laporan
                            <div class="pull-right">
                                <button class="btn btn-primary btn-xs" id="tagihan-now-refresh" data-url="<?=base_url('api/main/tagihan-now'.@$filter)?>"><i class="fa fa-refresh"></i> reload</button>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6 col-xs-6">
                                    <b>Periode</b>
                                </div>
                                <div class="col-md-6 col-xs-6">  
                                <div id="tagihan-now-periode"><?=$keterangan->tgl?></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-6">
                                    <b>Total Bayar</b>
                                </div>
                                <div class="col-md-6 col-xs-6">  
                                    <div id="tagihan-now-total"><?=$keterangan->total?></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-6">
                                    <b>Dana Terkumpul</b>
                                </div>
                                <div class="col-md-6 col-xs-6">  
                                    <div id="tagihan-now-terkumpul"><?=$keterangan->terkumpul?></div>
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-xs-6 col-xs-offset-6">  
                                    <div id="tagihan-now-kurang-bayar"><?=$keterangan->kurang_bayar?></div>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
            </div>
            <!-- /.col-lg-8 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->
</section>
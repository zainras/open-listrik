<section class="section-body">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Dashboard</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bar-chart-o fa-fw"></i> Area Chart Example
                        <div class="pull-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                    Actions
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="#">Action</a>
                                    </li>
                                    <li><a href="#">Another action</a>
                                    </li>
                                    <li><a href="#">Something else here</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <form method="POST">
                            <?=$msg?>
                            <div class="form-group">
                                <label>Nama Transaksi</label>
                                <input class="form-control" type="text" name="nama-transaksi" placeholder="Nama Transaksi" value="<?=$transaksi->name?>">
                            </div>
                            <div class="form-group">
                                <label>Nominal</label>
                                <input class="form-control" type="text" name="nominal-transaksi" placeholder="Nominal" value="<?=$transaksi->nominal?>">
                            </div>
                            <div class="form-group">
                                <select class="form-control" name="tipe-transaksi">
                                    <option value="">Pilih Jenis Transaksi</option>
                                    <option value="D" <?=($transaksi->type == 'D') ? 'selected' : ''?> >debet</option>
                                    <option value="K" <?=($transaksi->type == 'K') ? 'selected' : ''?> >kredit</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Deskripsi</label>
                                <textarea class="form-control" name="deskripsi-transaksi" placeholder="Deskripsi"><?=$transaksi->description?></textarea>
                            </div>
                            <?php //var_dump($transaksi->isdeleted);die; ?>
                            <div class="form-group">
                                <label>Hapus Data</label>
                                <input type="hidden" name="delete-transaksi" value="0" >
                                <input type="checkbox" name="delete-transaksi" value="1" <?=($transaksi->isdeleted == 1) ? 'checked' : ''?>>
                            </div>
                            <div class="form-group pull-right">
                                <button class="btn" type="reset">Reset</button>
                                <button class="btn btn-primary" type="submit" name="edit-transaksi" value="simpan">Simpan</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-times fa-fw"></i> History Perubahan Data
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                            <?php foreach ($transaksi->history as $key => $value) { ?>
                        <div class="list-group">
                            <div class="list-group-item">
                                <span>Nama : <?=$value['logs']->name?> </span>
                                <!-- <span>Nominal : <?=$value['logs']->nominal?> </span> -->
                                <!-- <span>Type : <?=$value['logs']->type?> </span> -->
                                <!-- <span>Nominal <?=$value['logs']->nominal?> </span> -->
                                <span class="pull-right text-muted small"><em><?=$value['created']?></em>
                                </span>
                            </div>
                            <div class="list-group-item">
                                <span>Nominal : <?=$value['logs']->nominal?> </span>
                                <span class="pull-right text-muted small"><em><?=$value['logs']->type?></em>
                                </span>
                            </div>
                            <div class="list-group-item">
                                <span><?=$value['logs']->description?> </span>
                            </div>
                        </div>
                            <?php } ?>
                        <!-- /.list-group -->
                        <a href="#" class="btn btn-default btn-block">View All Alerts</a>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-wrapper -->
</section>

<section class="section-body">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Dashboard</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/transaksi/add" class="btn btn-info">Tambah Transaksi</a>
                        <div class="pull-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                    Actions
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="<?=base_url('transaksi/')?>">Data Transaksi valid</a>
                                    </li>
                                    <li><a href="<?=base_url('transaksi/all-record')?>">Menampilkan Data terhapus</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table class="datatable table table-responsive table-bordered table-striped table-hover" data-url="api/main/datatable" data-type='transaksi-all'>
                            <thead>
                                <th>NO</th>
                                <th>Nama Transaksi</th>
                                <th>Nominal</th>
                                <th>Tipe</th>
                                <th>Tanggal Dihapus</th>
                                <!-- <th>Status</th> -->
                                <!-- <th>action</th> -->
                            </thead>
                        </table>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
            <!-- /.col-lg-8 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->
</section>

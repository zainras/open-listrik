<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// require APPPATH . 'libraries/Save_File.php';

class Tagihan extends Listrik_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('Mtagihan');
	}

	public $msg = '';

	public function index()
	{
		$this->load->helper('form');
		$data['tahun'] = $this->input->get('tahun');
		$data['bulan'] = $this->input->get('bulan');
		if ($this->input->get('filter')) {
			$data['filter'] = '?tahun='.$data['tahun'].'&bulan='.$data['bulan'].'&filter=tagihan';
		}
		$this->dataCss = 	[
								// '/assets/vendor/datatables/css/jquery.dataTables.min.css',
								'/assets/vendor/datatables/css/dataTables.bootstrap.min.css',
							];
		$this->dataJs = 	[
								'/assets/vendor/datatables/js/jquery.dataTables.min.js',
								'/assets/vendor/datatables/js/dataTables.bootstrap.min.js',
							];
		$data['keterangan'] = $this->Mtagihan->getTagihanNow();
		$this->render_view('tagihan-datatable',$data);
	}

	public function mobile()
	{
		$this->load->helper('form');
		$this->load->model('Mdatatable');
		$data['tahun'] = $this->input->get('tahun');
		$data['bulan'] = $this->input->get('bulan');
		if ($this->input->get('filter')) {
			$data['filter'] = '?tahun='.$data['tahun'].'&bulan='.$data['bulan'].'&filter=tagihan';
		}
		$this->dataCss = 	[
								// '/assets/vendor/datatables/css/jquery.dataTables.min.css',
								'/assets/vendor/datatables/css/dataTables.bootstrap.min.css',
							];
		$this->dataJs = 	[
								'/assets/vendor/datatables/js/jquery.dataTables.min.js',
								'/assets/vendor/datatables/js/dataTables.bootstrap.min.js',
							];
		$data['keterangan'] = $this->Mtagihan->getTagihanNow();
		$this->render_view('mobile-tagihan-datatable',$data);
	}


	public function mobile_offline()
	{
		$this->load->helper('form');
		$this->load->model('Mdatatable');
		$data['tahun'] = $this->input->get('tahun');
		$data['bulan'] = $this->input->get('bulan');
		if ($this->input->get('filter')) {
			$data['filter'] = '?tahun='.$data['tahun'].'&bulan='.$data['bulan'].'&filter=tagihan';
		}
		$this->dataCss = 	[
								'/assets/vendor/datatables/css/dataTables.bootstrap.min.css',
							];
		$this->dataJs = 	[
								'/assets/vendor/datatables/js/jquery.dataTables.min.js',
								'/assets/vendor/datatables/js/dataTables.bootstrap.min.js',
							];
		$data['keterangan'] = $this->Mtagihan->getTagihanNow();
		$this->render_view('offline-mobile-tagihan-datatable',$data);
	}

	public function update_data()
	{
		$this->Mtagihan->newData();
	}

	public function upload_tagihan()
	{
		$this->load->library('UploadToMysql');
		$this->load->helper('form');

		if (!empty($_POST)) {
			$config['upload_path'] = 'assets/upload/';
			$config['allowed_types'] = 'csv';
			$config['max_size']  = '1000';
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('upload-csv'))
			{
				$this->msg = msg('danger',$this->upload->display_errors());
			}
			else
			{
				$data = $this->upload->data();
				$csvData = $this->uploadtomysql->insert_from_csv($data['full_path']);
				$this->msg = msg($csvData['status'],$csvData['msg']);
			}
		}
		$data['msg'] = $this->msg;
		$this->render_view('tagihan-upload',$data);
	}

	public function tes()
	{
		$this->load->library('Save_File');
		$a = $this->Save_File->getFile();
		var_dump($a);
		// var_dump($this->getFile());die;
	}

}

/* End of file Tagihan.php */
/* Location: ./application/controllers/Tagihan.php */
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Listrik_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$data = [];
		$this->dataCss = ['assets/css/main.css'];
		$this->dataJs = [
			'assets/vendor/moment-with-locales/moment-with-locales.min.js',
			'assets/vendor/raphael/raphael.min.js',
			'assets/vendor/morrisjs/morris.min.js',
			'assets/vendor/chartjs/Chart.min.js',
			'assets/js/morris-data.js',
			'assets/js/main.js',
		];	
		$this->render_view('dashboard',$data);
	}
}

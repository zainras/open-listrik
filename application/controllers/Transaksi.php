<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi extends Listrik_Controller {

	public $msg = '';

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	public function index()
	{
		
		$data = [];
		$this->dataCss = 	[
								// '/assets/vendor/datatables/css/jquery.dataTables.min.css',
								'/assets/vendor/datatables/css/dataTables.bootstrap.min.css',
							];
		$this->dataJs = 	[
								'/assets/vendor/datatables/js/jquery.dataTables.min.js',
								'/assets/vendor/datatables/js/dataTables.bootstrap.min.js',
							];
		$this->render_view('transaksi-datatable',$data);	
	}
	public function all_record()
	{
		
		$data = [];
		$this->dataCss = 	[
								// '/assets/vendor/datatables/css/jquery.dataTables.min.css',
								'/assets/vendor/datatables/css/dataTables.bootstrap.min.css',
							];
		$this->dataJs = 	[
								'/assets/vendor/datatables/js/jquery.dataTables.min.js',
								'/assets/vendor/datatables/js/dataTables.bootstrap.min.js',
							];
		$this->render_view('transaksi-all-datatable',$data);	
	}
	public function add()
	{
		$this->load->model('Mtransaksi');
		$this->load->helper('form');
		$this->load->library('form_validation');
		if ($this->input->post('simpan-transaksi'))
		{

			$this->form_validation->set_rules('nama-transaksi', 'Nama Transaksi ', 'trim|required');
			$this->form_validation->set_rules('tipe-transaksi', 'Jenis Transaksi ', 'trim|required');
			$this->form_validation->set_rules('nominal-transaksi', 'Nominal', 'trim|required|numeric');
			$this->form_validation->set_rules('deskripsi-transaksi', 'Deskripsi', 'trim');

	        if ($this->form_validation->run() == TRUE)
	        {
	        	$data = [
	        		'name' => $this->input->post('nama-transaksi',true),
	        		'type' => $this->input->post('tipe-transaksi',true),
	        		'nominal' => $this->input->post('nominal-transaksi',true),
	        		'deskripsi' => $this->input->post('deskripsi-transaksi',true),
	        	];
	            $this->Mtransaksi->addTransaksi($data);
	            $this->msg = msg('success','Berhasil Menambahkan Pelanggan');
	        }
	        else
	        {
	            $this->msg = msg('danger',validation_errors());
	        }
	    }
		$data = [
			'msg' =>$this->msg,
		];
		$this->render_view('transaksi-add',$data);	
	}


	public function edit($id=null)
	{
		if (empty($id))
		{
			show_404();
		}
		else
		{
		$this->load->model('Mtransaksi');
		$this->load->helper('form');
		$this->load->library('form_validation');

		$dtTransaski = $this->Mtransaksi->getHistoryTransaksi($id);
		// var_dump($dtTransaski);die;
		if (empty($dtTransaski)) 
		{
			show_404();
		}
		// var_dump($this->input->post());die;
		if ($this->input->post('edit-transaksi'))
		{
			$this->form_validation->set_rules('nama-transaksi', 'Nama Transaksi ', 'trim|required');
			$this->form_validation->set_rules('tipe-transaksi', 'Jenis Transaksi ', 'trim|required');
			// $this->form_validation->set_rules('delete-transaksi', 'Hapus Transaksi ', 'trim|required');
			$this->form_validation->set_rules('nominal-transaksi', 'Nominal', 'trim|required|numeric');
			$this->form_validation->set_rules('deskripsi-transaksi', 'Deskripsi', 'trim');

			$dtDelete = $this->input->post('delete-transaksi',true);
			// sanitize input 
			$delete = ($dtDelete == 1) ? 1 : 0;

	        if ($this->form_validation->run() == TRUE)
	        {
	        	$update = [
	        		'name' => $this->input->post('nama-transaksi',true),
	        		'type' => $this->input->post('tipe-transaksi',true),
	        		'nominal' => $this->input->post('nominal-transaksi',true),
	        		'description' => $this->input->post('deskripsi-transaksi',true),
	        		'isdeleted' => $delete,
	        	];
	            $this->Mtransaksi->editTransaksi($id,$update,$dtTransaski);
	            $this->msg = msg('success','Berhasil Mengubah Transaksi');
	        }
	        else
	        {
	            $this->msg = msg('danger',validation_errors());
	        }
	    }
		
		$data = [
			'msg' 		=> $this->msg,
			'transaksi'	=> $dtTransaski,
		];
		$this->render_view('transaksi-edit',$data);
		}
	}

	// private function _validationIsDeleted($delete)
	// {
	// 	if () {
	// 		# code...
	// 	}
	// }

}

/* End of file Transaksi.php */
/* Location: ./application/controllers/Transaksi.php */
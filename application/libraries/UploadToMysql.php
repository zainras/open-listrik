<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UploadToMysql
{
	protected $ci;

	public function __construct()
	{
        $this->ci =& get_instance();
	}

	public function insert_from_csv($file)
	{
		$insert = [];
		$csv = array_map('str_getcsv', file($file));
		$now = date('Y-m-d H:i:s');

		// check header (row 1) is match
		if (count($this->_this_month()) == 0) {
			if ($csv[0][0] == 'no_rekening' && $csv[0][1] == 'nominal'){
				// delete row 1
				unset($csv[0]);
				// loop data
				foreach ($csv as $key => $value) {
				    if ($value[1] != '')
				    {
				        $insert[]=[
				        	'no_rekening' 	=> $value[0],
				        	'nominal' 		=> $value[1],
				        	'periode' 		=> $now,
				        	'modified' 		=> $now,
				        ];
				    }
				}
				if (!empty($insert))
				{
					$this->ci->db->insert_batch('tagihan', $insert);
					$row = $this->ci->db->affected_rows();
					$ret['status'] = 'success';
					$ret['msg'] = 'berhasil menambahkan '.$row.' data';
				}
				else
				{
					$ret['status'] = 'danger';
					$ret['msg'] = 'gagal menambahkan data';
				}
			}
			else
			{
				$ret['status'] = 'danger';
				$ret['msg'] = 'format csv tidak sesuai silahkan baca dokumentasi';
			}
		}
		else
		{
			$ret['status'] = 'danger';
			$ret['msg'] = 'data untuk bulan ini sudah ada';
		}
		return $ret;
	}

	private function _this_month()
	{
		$this->ci->db->select('periode');
		$this->ci->db->from('tagihan');
		$this->ci->db->where('MONTH(periode)', 'MONTH(CURRENT_DATE())',false);
		$this->ci->db->where('YEAR(periode)', 'YEAR(CURRENT_DATE())',false);
		$q = $this->ci->db->get();
		$r = $q->result();
		return $r;
	}

	

}

/* End of file UploadToMysql.php */
/* Location: ./application/libraries/UploadToMysql.php */

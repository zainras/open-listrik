<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Save_File extends CI_Controller 
{

	public function __construct()
	{
        $this->ci =& get_instance();
	}

	public function getFile()
	{
		$get = file_get_contents(APPPATH . '/cache/save/tableOffline.json');
		return $get;
	}	

	public function saveFile($data)
	{

		$this->ci->load->helper('file');

		$path = APPPATH . 'cache/save/tableOffline.json';


		if ( ! write_file($path, $data))
		{
		        return 'Unable to write the file';
		}
		else
		{
		        return 'File written!';
		}
	}

}

/* End of file Save_File.php */
/* Location: ./application/libraries/Save_File.php */

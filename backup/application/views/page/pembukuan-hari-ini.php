<section class="section-body">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Pembukuan</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bar-chart-o fa-fw"></i> Pembukuan Verifikasi
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table class="datatable table table-responsive table-bordered table-striped table-hover" data-url="api/main/datatable" data-type='pembukuan-verifikasi'>
                            <thead>
                                <!-- <th>NO Rekening</th> -->
                                <th></th>
                                <th>Tanggal</th>
                                <th>Keterangan</th>
                                <th>Kredit</th>
                                <th>Debet</th>
                                <th>Saldo</th>
                            </thead>
                        </table>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
            <!-- /.col-lg-8 -->
        </div>
    </div>
    <!-- /#page-wrapper -->
</section>

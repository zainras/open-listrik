<section class="section-body">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Semua Data Pelanggan</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <a href="/pelanggan/add" class="btn btn-info">Tambah Pelanggan</a>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table class="datatable table table-responsive table-bordered table-striped table-hover" data-controller="pelanggan" data-method="all-data">
                            <thead>
                                <th>NO Rekening</th>
                                <th>Nama Lengkap</th>
                                <th>Atas Nama</th>
                                <th>Nomor HP</th>
                                <th>Tanggal Ditambahkan</th>
                                <th>Status</th>
                                <th>action</th>
                            </thead>
                        </table>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
            <!-- /.col-lg-8 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->
</section>

<section class="section-body">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Dashboard</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-bar-chart-o fa-fw"></i> Area Chart Example
                        <div class="pull-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                    Actions
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="#">Action</a>
                                    </li>
                                    <li><a href="#">Another action</a>
                                    </li>
                                    <li><a href="#">Something else here</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li><a href="#">Separated link</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <table class="datatable table table-responsive table-bordered table-striped table-hover" data-url="api/main/datatable" data-type='tagihan-mobile'>
                            <thead>
                                <!-- <th>NO Rekening</th> -->
                                <th></th>
                                <th>Nama Lengkap</th>
                                <th>Nominal</th>
                                <!-- <th>Status</th> -->
                                <th>action</th>
                            </thead>
                        </table>
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
            <!-- /.col-lg-8 -->
        </div>
                <div id="tagihan-now">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Laporan
                            <div class="pull-right">
                                <button class="btn btn-primary btn-xs" id="tagihan-now-refresh"><i class="fa fa-refresh"></i> reload</button>
                            </div>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6 col-xs-6">
                                    <b>Periode</b>
                                </div>
                                <div class="col-md-6 col-xs-6">  
                                <div id="tagihan-now-periode"><?=$keterangan->tgl?></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-6">
                                    <b>Total Bayar</b>
                                </div>
                                <div class="col-md-6 col-xs-6">  
                                    <div id="tagihan-now-total"><?=$keterangan->total?></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-xs-6">
                                    <b>Dana Terkumpul</b>
                                </div>
                                <div class="col-md-6 col-xs-6">  
                                    <div id="tagihan-now-terkumpul"><?=$keterangan->terkumpul?></div>
                                </div>
                            </div>
                            <hr />
                            <div class="row">
                                <div class="col-xs-6 col-xs-offset-6">  
                                    <div id="tagihan-now-kurang-bayar"><?=$keterangan->kurang_bayar?></div>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
        <!-- /.row -->
    </div>
    <!-- /#page-wrapper -->
</section>

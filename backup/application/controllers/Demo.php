<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Demo extends LISTRIK_Controller {

	public function index()
	{
		$this->load->model('Mdatatable');
		$this->load->model('Mtagihan');
		$data = [];

		if(!empty($this->input->post('getDatatable')))
		{
			$data = $this->Mdatatable->resultData();
			echo json_encode($data);
			return;
		}
		$this->dataCss = 	[
								// '/assets/vendor/datatables/css/jquery.dataTables.min.css',
								'/assets/vendor/datatables/css/dataTables.bootstrap.min.css',
							];
		$this->dataJs = 	[
								'/assets/vendor/datatables/js/jquery.dataTables.min.js',
								'/assets/vendor/datatables/js/dataTables.bootstrap.min.js',
							];
		$data['keterangan'] = $this->Mtagihan->getTagihanNow();
		$this->render_view('tagihan-datatable',$data);
	}

}

/* End of file Demo.php */
/* Location: ./application/controllers/Demo.php */
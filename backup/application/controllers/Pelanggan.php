<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelanggan extends Listrik_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Mpelanggan');
	}

	public $msg = '';
	public function index()
	{
		$data['pageTitle'] = 'Home';	
		if(!empty($this->input->post('getDatatable')))
		{
			$data = $this->Mpelanggan->getDatatable();
			echo json_encode($data);
			return;
		}
		$this->dataCss = 	[
								// '/assets/vendor/datatables/css/jquery.dataTables.min.css',
								'/assets/vendor/datatables/css/dataTables.bootstrap.min.css',
							];
		$this->dataJs = 	[
								'/assets/vendor/datatables/js/jquery.dataTables.min.js',
								'/assets/vendor/datatables/js/dataTables.bootstrap.min.js',
							];
		$this->render_view('pelanggan-datatable',$data);
	}
	public function add()
	{
		$this->load->library('form_validation');
		if ($this->input->post('simpan-pelanggan'))
		{

			$this->form_validation->set_rules('nama-pelanggan', 'Nama', 'trim|required');
			$this->form_validation->set_rules('atas-nama-pelanggan', 'Atas Nama', 'trim|required');
			$this->form_validation->set_rules('no-rekening-pelanggan', 'Nomor Rekening', 'trim|required|numeric|min_length[6]|max_length[12]');
			$this->form_validation->set_rules('nohp-pelanggan', 'Nomor HP', 'trim|required|min_length[10]|max_length[15]');
			$this->form_validation->set_rules('alamat-pelanggan', 'Alamat', 'trim');

	        if ($this->form_validation->run() == TRUE)
	        {
	            $this->Mpelanggan->addPelanggan();
	            $this->msg = msg('success','Berhasil Menambahkan Pelanggan');
	        }
	        else
	        {
	            $this->msg = msg('danger',validation_errors());
	        }
	    }
		$data['msg'] = $this->msg;
		$this->render_view('pelanggan-add',$data);
	}

	public function view($id=null)
	{
		$cek = $this->Mpelanggan->cekId($id);
		if ($cek == true) {
			$data['pelanggan'] = $this->Mpelanggan->getPelanggan($id);
			$this->render_view('pelanggan-view',$data);
		} else {
			show_404();
		}
		
	}

	public function edit($id)
	{
		$this->load->library('form_validation');
		if ($this->input->post('edit-pelanggan'))
		{
			$this->form_validation->set_rules('nama-pelanggan', 'Nama', 'trim|required');
			$this->form_validation->set_rules('atas-nama-pelanggan', 'Atas Nama', 'trim|required');
			$this->form_validation->set_rules('no-rekening-pelanggan', 'Nomor Rekening', 'trim|required|numeric|min_length[6]|max_length[15]');
			$this->form_validation->set_rules('nohp-pelanggan', 'Nomor HP', 'trim|min_length[10]|max_length[15]');
			$this->form_validation->set_rules('alamat-pelanggan', 'Alamat', 'trim');

	        if ($this->form_validation->run() == TRUE)
	        {
	            $this->Mpelanggan->editPelanggan($id);
	            $this->msg = msg('success','Berhasil Mengubah Data Pelanggan');
	        }
	        else
	        {
	            $this->msg = msg('danger',validation_errors());
	        }
		}

        $data['pageTitle']	= 'Edit Pelanggan';
        $data['pelanggan'] 	= $this->Mpelanggan->getPelanggan($id);
        $data['msg']		= $this->msg;
	    $this->render_view('pelanggan-edit',$data);
	}

	public function delete($id)
	{
		$this->Mpelanggan->deletePelanggan($id);
		redirect('pelanggan','refresh');
	}

	public function all_data()
	{
		$data['pageTitle'] = 'Home';	
		if(!empty($this->input->post('getDatatable')))
		{
			$data = $this->Mpelanggan->getDatatable('all');
			echo json_encode($data);
			return;
		}
		$this->dataCss = 	[
								// '/assets/vendor/datatables/css/jquery.dataTables.min.css',
								'/assets/vendor/datatables/css/dataTables.bootstrap.min.css',
							];
		$this->dataJs = 	[
								'/assets/vendor/datatables/js/jquery.dataTables.min.js',
								'/assets/vendor/datatables/js/dataTables.bootstrap.min.js',
							];

		$data['pegeTitle'] = 'Semua Pelanggan';
		$this->render_view('pelanggan-semua-datatable',$data);
	}
}

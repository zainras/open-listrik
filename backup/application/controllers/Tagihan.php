<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tagihan extends Listrik_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('Mtagihan');
	}

	public $msg = '';

	public function index()
	{
		$this->load->helper('form');
		if ($this->input->get('filter')) {
			$data['tahun'] = $this->input->get('tahun');
			$data['bulan'] = $this->input->get('bulan');
			$data['filter'] = '?tahun='.$data['tahun'].'&bulan='.$data['bulan'].'&filter=tagihan';
		}
		$this->dataCss = 	[
								// '/assets/vendor/datatables/css/jquery.dataTables.min.css',
								'/assets/vendor/datatables/css/dataTables.bootstrap.min.css',
							];
		$this->dataJs = 	[
								'/assets/vendor/datatables/js/jquery.dataTables.min.js',
								'/assets/vendor/datatables/js/dataTables.bootstrap.min.js',
							];
		$data['keterangan'] = $this->Mtagihan->getTagihanNow();
		$this->output->cache(100);
		$this->render_view('tagihan-datatable',$data);
	}

	public function super_edit()
	{
		$this->load->helper('form');
		if ($this->input->get('filter')) {
			$data['tahun'] = $this->input->get('tahun');
			$data['bulan'] = $this->input->get('bulan');
			$data['filter'] = '?tahun='.$data['tahun'].'&bulan='.$data['bulan'].'&filter=tagihan';
		}
		$this->dataCss = 	[
								// '/assets/vendor/datatables/css/jquery.dataTables.min.css',
								'/assets/vendor/datatables/css/dataTables.bootstrap.min.css',
							];
		$this->dataJs = 	[
								'/assets/vendor/datatables/js/jquery.dataTables.min.js',
								'/assets/vendor/datatables/js/dataTables.bootstrap.min.js',
							];
		$data['keterangan'] = $this->Mtagihan->getTagihanNow();
		$this->render_view('tagihan-super-datatable',$data);
	}

	public function mobile()
	{
		$this->load->model('Mdatatable');
		$this->dataCss = 	[
								// '/assets/vendor/datatables/css/jquery.dataTables.min.css',
								'/assets/vendor/datatables/css/dataTables.bootstrap.min.css',
							];
		$this->dataJs = 	[
								'/assets/vendor/datatables/js/jquery.dataTables.min.js',
								'/assets/vendor/datatables/js/dataTables.bootstrap.min.js',
							];
		$data['keterangan'] = $this->Mtagihan->getTagihanNow();
		$this->render_view('mobile-tagihan-datatable',$data);
	}

	public function update_data()
	{
		$this->Mtagihan->newData();
	}

}

/* End of file Tagihan.php */
/* Location: ./application/controllers/Tagihan.php */
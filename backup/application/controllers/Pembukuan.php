<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembukuan extends Listrik_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('Mtagihan');
	}

	public $msg = '';

	public function index()
	{
		$this->dataCss = 	[
								// '/assets/vendor/datatables/css/jquery.dataTables.min.css',
								'/assets/vendor/datatables/css/dataTables.bootstrap.min.css',
							];
		$this->dataJs = 	[
								'/assets/vendor/datatables/js/jquery.dataTables.min.js',
								'/assets/vendor/datatables/js/dataTables.bootstrap.min.js',
							];
		$data['keterangan'] = $this->Mtagihan->getTagihanNow();
		$this->render_view('pembukuan-datatable',$data);
	}

	public function hari_ini()
	{
		$this->dataCss = 	[
								// '/assets/vendor/datatables/css/jquery.dataTables.min.css',
								'/assets/vendor/datatables/css/dataTables.bootstrap.min.css',
							];
		$this->dataJs = 	[
								'/assets/vendor/datatables/js/jquery.dataTables.min.js',
								'/assets/vendor/datatables/js/dataTables.bootstrap.min.js',
							];
		$data['tes'] = '';
		$this->render_view('pembukuan-hari-ini',$data);	
	}

}

/* End of file Pembukuan.php */
/* Location: ./application/controllers/Pembukuan.php */
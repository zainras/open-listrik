<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mpelanggan extends CI_Model {

	public function getPelanggan($id)
	{
		$this->db->select('*');
		$this->db->from('pelanggan');
		$this->db->where('no_rekening', $id);
		$q = $this->db->get();
		$r = $q->row();
		return $r;
	}

	public function addPelanggan()
	{
		$ins = [
			'no_rekening' 	=> $this->input->post('no-rekening-pelanggan',true),
			'nama_lengkap' 	=> $this->input->post('nama-pelanggan',true),
			'atas_nama' 	=> $this->input->post('atas-nama-pelanggan',true),
			'alamat' 		=> $this->input->post('alamat-pelanggan',true),
			'nomor_hp'		=> $this->input->post('nohp-pelanggan',true),
			'created' 		=> date('Y-m-d H:i:s'),
			'modified' 		=> date('Y-m-d H:i:s'),
			'status' 		=> 1,
		];
		$this->db->insert('pelanggan', $ins);
	}

	public function editPelanggan($id)
	{
		$update = [
			'no_rekening' 	=> $this->input->post('no-rekening-pelanggan',true),
			'nama_lengkap' 	=> $this->input->post('nama-pelanggan',true),
			'atas_nama' 	=> $this->input->post('atas-nama-pelanggan',true),
			'alamat' 		=> $this->input->post('alamat-pelanggan',true),
			'nomor_hp'		=> $this->input->post('nohp-pelanggan',true),
			'modified' 		=> date('Y-m-d H:i:s'),
			'status' 		=> 1,
		];
		$this->db->where('no_rekening', $id);
		$this->db->update('pelanggan', $update);
	}

	public function deletePelanggan($id)
	{
		$delete['isdelete'] = 1;
		$delete['modified'] = date('Y-m-d H:i:s');
		$delete['status']	= 0;
		$this->db->where('no_rekening', $id);
		$this->db->update('pelanggan', $delete);
	}

	public function cancelDeletePelanggan($id)
	{
		$cancel['isdelete'] = 0;
		$cancel['modified'] = date('Y-m-d H:i:s');
		$this->db->where('no_rekening', $id);
		$this->db->update('pelanggan', $cancel);
	}

	public function cekId($id)
	{
		$ret = false;
		$this->db->where('no_rekening', $id);
		$cek = $this->db->count_all_results('pelanggan');
		if ($cek > 0) {
			$ret = true;
		}
		return $ret;
	}

	public function getDatatable($type='aktif',$list=false,$filter=false,$defaultOrder=false,$action=false)
	{
			switch ($type) {
			case 'aktif':
				$filter = 	[
								['where'=>'isdelete','key'=>'0']
							];
				$action = true;
				break;
			case 'all':
				$defaultOrder = ['isdelete','desc'];
				break;
			default:
				# code...
				break;
		}
		if (!$list)
		{
			$list = [
						['select' => 'no_rekening','as' => 'no'],
						['select' => 'nama_lengkap','as' => 'nama'],
						['select' => 'atas_nama','as' => 'an'],
						['select' => 'nomor_hp','as' => 'nohp'],
						['select' => 'created','as' => 'tgl'],
						['select' => 'status','as' => 'status'],
					];
		}
		if (!$filter)
		{
			$filter = 	[];
		}
		if (!$defaultOrder)
		{
			$defaultOrder = ['atas_nama','asc'];
		}


		$start = $this->input->post('start');
		$limit = $this->input->post('length');
		if($start !=null && $limit !=null){
			$this->db->limit($limit,$start);
		}

		$search = $this->input->post('search');
		if ($search['value'] !=''){
			$key = $search['value']; // data transaksi not deleted
			// $key 	= $this->db->escape_like_str($search['value']);
			$this->db->group_start();
			foreach ($list as $ls) {
				$this->db->or_like($ls['select'], $key);
			}			
			$this->db->group_end();
		}
		$order = $this->input->post('order');
		$column = isset($order[0]['column'])?$order[0]['column']:-1;
		if($column >= 0 && $column < count($list)){
			$ord = $list[$column]['select'];
			$by = $order[0]['dir'];
			$this->db->order_by($ord , $by);
		} else {
			$ord 	= $defaultOrder[0];
			$by 	= $defaultOrder[1];
			$this->db->order_by($ord , $by);
		}

		$this->db->select("SQL_CALC_FOUND_ROWS ".$list[0]['select'] ,FALSE);
		//dynamic select
		foreach($list as $key => $ls) { 
			$this->db->select($ls['select'].' as '.$ls['as']);
		}
		// ex : $this->db->select('transaksi.name');

		$this->db->from('pelanggan');

		//dynamic filter
		foreach($filter as $key => $fil) {
			$this->db->where($fil['where'],$fil['key']);
		}
		
		$sql = $this->db->get();
		$q = $sql->result_array();
		$this->db->select("FOUND_ROWS() AS total_row");
		$row = $this->db->get()->row();

		$sEcho = $this->input->post('draw');
		$getCountAll = $this->db->count_all_results('pelanggan');
		$output = array(
			"draw" => intval($sEcho),
			"recordsTotal" => $getCountAll,
			"recordsFiltered" => $row->total_row,
			"data" => array()
		);

		foreach ($q as $val) {
		$act ='<a href="/pelanggan/view/'.$val['no_rekening'].'" class="btn btn-xs btn-datatable btn-primary">Lihat</a>';
		$act .= '<a href="/pelanggan/delete/'.$val['no_rekening'].'" class="btn btn-del btn-xs btn-datatable btn-danger">Hapus</a>';
		$res = [];
			foreach($list as $ls) {
				$alias = $ls['as'];	
				$res[] = $val[$alias];
			}
		if ($action!=false) {
			$res[] = $act;
		}

		$output['data'][] = $res;
		
		}
		return $output;
	}

}

/* End of file Mpelanggan.php */
/* Location: ./application/models/Mpelanggan.php */
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdatatable extends Listrik_Model {

	public function dataTagihan()
	{

		$config['list'] 	= 	[
									['select' => 'p.no_rekening as no','as'=>'no'],
									['select' => 'p.nama_lengkap as nama','as'=>'nama'],
									['select' => 'p.atas_nama as an','as'=>'an'],
									['select' => 'ROUND(t.nominal,-3) as nominal','as'=>'nominal'],
									['select' => 'DATE_FORMAT(t.periode, "%M %Y") as tgl','as'=>'tgl'],
									['select' => 't.status as status','as'=>'status'],
								];
		$config['filter'] 	= 	[
									['key'=>'p.isdelete','value'=>'0'],
									['key'=>'p.status','value'=>'1'],
								];
		$config['from']		=	'pelanggan as p';
		$config['join'] 	= 	[
									['table'=>'tagihan as t','cond'=>'p.no_rekening=t.no_rekening','type'=>'left'],
								];
		$config['order'] 	= 	['t.modified','desc'];
		return $config;
	}

	public function getDatatable($type)
	{
		switch ($type) {
			case 'tagihan':
				$ret = $this->_tagihan();
				break;
			case 'tagihan-mobile':
				$ret = $this->_tagihanMobile();
				break;
			case 'pembukuan':
				$ret = $this->_pembukuan();
				break;
			case 'pembukuan-verifikasi':
				$ret = $this->_pembukuan_verifikasi();
				break;
			default:
				# code...
				break;
		}

		return $ret;
	}

	private function _tagihan()
	{
		$config['list'] 	= 	[
									['select' => 't.id','as'=>'id'],
									['select' => 'p.no_rekening','as'=>'no'],
									['select' => 'p.nama_lengkap','as'=>'nama'],
									['select' => 'p.atas_nama','as'=>'an'],
									['select' => 't.nominal','as'=>'nomRaw'],
									['select' => 'ROUND(t.nominal,-3)','as'=>'nomRound'],
									['select' => 'ROUND(t.nominal,-3)+2000','as'=>'nom2rb'],
									['select' => 'DATE_FORMAT(t.periode, "%M %Y")','as'=>'tgl'],
									['select' => 'IF(t.status=1,"lunas","belum lunas")','as'=>'status_string'],
									['select' => 't.status','as'=>'status'],
								];
		// var_dump($this->input->post());die;
		if ($this->input->get('filter'))
		{
			$tahun = $this->input->get('tahun',true);
			$bulan = $this->input->get('bulan',true);
			$config['filter'] 	= 	[
										['key'=>'p.isdelete','value'=>'0'],
										['key'=>'p.status','value'=>'1'],
										['key'=>'t.periode >=','value'=>tahunBulan($tahun,$bulan,'awal')],
										['key'=>'t.periode <','value'=>tahunBulan($tahun,$bulan,'akhir')],
									];
			
		} else {
			$config['filter'] 	= 	[
										['key'=>'p.isdelete','value'=>'0'],
										['key'=>'p.status','value'=>'1'],
									];
			
		}
		$config['from']		=	'pelanggan as p';
		$config['join'] 	= 	[
									['table'=>'tagihan as t','cond'=>'p.no_rekening=t.no_rekening','type'=>'left'],
								];
		$config['order'] 	= 	['t.id','asc'];

		//process in core
		$out = $this->proscessData($config);
		
		$q = $out['q'];
		$output = $out['out'];
		
		foreach ($q as $val)
		{
			if ($val['status'] == 1) {
				$act = '<button data-href="/api/main/datatable?id='.$val['id'].'&id_status='.$val['status'].'" class="btn btn-xs btn-datatable btn-danger btn-edit">Batalkan</button>';
			}
			if ($val['status'] == 0) {
				$act = '<button data-href="/api/main/datatable?id='.$val['id'].'&id_status='.$val['status'].'" class="btn btn-xs btn-datatable btn-primary btn-edit">Bayar</button>';
			}
	
			
		
			$status = ($val['status'] == 1) ? 'lunas' : 'belum lunas';
			$output['data'][] = 
				[
					$val['id'],
					$val['no'],
					$val['nama'],
					$val['an'],
					rupiah($val['nomRaw']),
					rupiah($val['nomRound']),
					rupiah($val['nom2rb']),
					$val['tgl'],
					$val['status_string'],
					$act,
				];
		
		}
		return $output;
	}

	private function _tagihanMobile()
	{
		$config['list'] 	= 	[
									['select' => 'p.no_rekening','as'=>'no'],
									['select' => 't.id','as'=>'id'],
									['select' => 'p.nama_lengkap','as'=>'nama'],
									['select' => 'ROUND(t.nominal,-3)','as'=>'nominal'],
									['select' => 't.status','as'=>'status'],
								];
		if ($this->input->get('filter'))
		{
			$tahun = $this->input->get('tahun',true);
			$bulan = $this->input->get('bulan',true);
			$config['filter'] 	= 	[
										['key'=>'p.isdelete','value'=>'0'],
										['key'=>'p.status','value'=>'1'],
										['key'=>'t.periode >=','value'=>tahunBulan($tahun,$bulan,'awal')],
										['key'=>'t.periode <','value'=>tahunBulan($tahun,$bulan,'akhir')],
									];
			
		} else {
			$config['filter'] 	= 	[
										['key'=>'p.isdelete','value'=>'0'],
										['key'=>'p.status','value'=>'1'],
										['key'=>'t.status','value'=>'0'],
									];
			
		}
		$config['from']		=	'pelanggan as p';
		$config['join'] 	= 	[
									['table'=>'tagihan as t','cond'=>'p.no_rekening=t.no_rekening','type'=>'left'],
								];
		$config['order'] 	= 	['t.id','asc'];

		//process in core
		$out = $this->proscessData($config);
		
		$q = $out['q'];
		$output = $out['out'];
		
		foreach ($q as $val)
		{
			if ($val['status'] == 1) {
				$act = '<button data-href="/api/main/datatable?id='.$val['id'].'&id_status='.$val['status'].'" class="btn btn-xs btn-datatable btn-danger btn-edit">Batalkan</button>';
			}
			if ($val['status'] == 0) {
				$act = '<button data-href="/api/main/datatable?id='.$val['id'].'&id_status='.$val['status'].'" class="btn btn-xs btn-datatable btn-primary btn-edit">Bayar</button>';
			}
	
			
		
			$status = ($val['status'] == 1) ? 'lunas' : 'belum lunas';
			$output['data'][] = 
				[
					$val['id'],
					$val['nama'],
					rupiah($val['nominal']),
					// $status,
					$act,
				];
		
		}
		return $output;
	}

	private function _pembukuan()
	{
		$config['list'] 	= 	[
									['select' => 'trans.id','as'=>'id'],
									['select' => 'trans.created','as'=>'tgl'],
									['select' => 'trans.id_resource','as'=>'id_res'],
									['select' => 'IF(trans.type="K",trans.nominal,null)','as'=>'kredit'],
									['select' => 'IF(trans.type="D",trans.nominal,null)','as'=>'debet'],
									// ['select' => 'SUM(saldo,debet)','as'=>'nama'],
								];
		$config['filter'] 	= 	[
									// ['key'=>'p.isdelete','value'=>'0'],
									// ['key'=>'p.status','value'=>'1'],
									// ['key'=>'t.periode >=','value'=>tahunBulan($tahun,$bulan,'awal')],
									// ['key'=>'t.periode <','value'=>tahunBulan($tahun,$bulan,'akhir')],
								];
		$config['from']		=	'transaksi as trans';
		// $config['join'] 	= 	[
		// 							['table'=>'tagihan as t','cond'=>'p.no_rekening=t.no_rekening','type'=>'left'],
		// 						];
		$config['order'] 	= 	['trans.id','asc'];

		//process in core
		$out = $this->proscessData($config);
		
		$q = $out['q'];
		$output = $out['out'];
		
		foreach ($q as $val)
		{
	
			$output['data'][] = 
				[
					$val['id'],
					$val['tgl'],
					$val['id_res'],
					$val['kredit'],
					$val['debet'],
					'saldo',
					$val['id'],
					$val['id'],
				];
		
		}
		return $output;
	}

	private function _pembukuan_verifikasi()
	{
		$config['list'] 	= 	[
									['select' => 'trans.id','as'=>'id'],
									['select' => 'trans.created','as'=>'tgl'],
									['select' => 'trans.id_resource','as'=>'id_res'],
									['select' => 'IF(trans.type="K",trans.nominal,null)','as'=>'kredit'],
									['select' => 'IF(trans.type="D",trans.nominal,null)','as'=>'debet'],
									// ['select' => 'SUM(saldo,debet)','as'=>'nama'],
								];
		$config['filter'] 	= 	[
									// ['key'=>'p.isdelete','value'=>'0'],
									// ['key'=>'p.status','value'=>'1'],
									// ['key'=>'t.periode >=','value'=>tahunBulan($tahun,$bulan,'awal')],
									// ['key'=>'t.periode <','value'=>tahunBulan($tahun,$bulan,'akhir')],
								];
		$config['from']		=	'tagihan as tagih';
		$config['join'] 	= 	[
									['table'=>'transaksi as trans','cond'=>'tagih.id=trans.id_resource','type'=>'left'],
								];
		$config['order'] 	= 	['trans.id','asc'];

		//process in core
		$out = $this->proscessData($config);
		
		$q = $out['q'];
		$output = $out['out'];
		
		foreach ($q as $val)
		{
	
			$output['data'][] = 
				[
					$val['id'],
					$val['id'],
					$val['id_res'],
					$val['kredit'],
					$val['debet'],
					'saldo',
					$val['id'],
					$val['id'],
				];
		
		}
		return $output;
	}
}

/* End of file Mdatatable.php */
/* Location: ./application/models/Mdatatable.php */
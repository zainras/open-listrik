<?php
defined('BASEPATH') OR exit('No direct script access allowed');

abstract class Listrik_Controller extends CI_Controller {

	public $dataCss = [];
	public $dataJs = [];

    public function __construct()
    {
            parent::__construct();
            // Your own constructor code
    }

    public function render_view($view,$data)
    {
    	$dtHeader['cssFile'] = $this->_processCss();
    	$dtHeader = array_merge($dtHeader,$data);

    	$dtFooter['jsFile'] = $this->_processJs();
    	$dtFooter = array_merge($dtFooter,$data);
    	
    	$this->load->view('layouts/header', $dtHeader);
    	$this->load->view('layouts/sidebar', $data);
    	$this->load->view('page/'.$view, $data);
    	$this->load->view('layouts/footer', $dtFooter);
    }

    public function _processCss()
    {
    	$ret = '';
    	if ($this->dataCss != []) {
	    	foreach ($this->dataCss as $key => $value) {
	    		$ret .= '<link href="'.$value.'" rel="stylesheet">';
	    	}
	    }
    	return $ret;
    }

    public function _processJs()
    {
    	$ret = '';
    	if ($this->dataJs != []) {
	    	foreach ($this->dataJs as $key => $value) {
	    		$ret .= '<script src="'.$value.'"></script>';
	    	}
    	}
    	return $ret;
    }
}

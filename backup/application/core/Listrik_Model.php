<?php
defined('BASEPATH') OR exit('No direct script access allowed');

abstract class Listrik_Model extends CI_Model {

    public function __construct()
    {
            parent::__construct();
            // Your own constructor code
    }
    public function proscessData($config)
    {
        // $config = $this->dataTagihan();
        $list   = $config['list'];
        $filter = $config['filter'];
        $join   = (isset($config['join'])) ? $config['join'] : null;
        $sort   = $config['order'];

        $start = $this->input->post('start');
        $limit = $this->input->post('length');
        if($start !=null && $limit !=null){
            $this->db->limit($limit,$start);
        }

        $search = $this->input->post('search');
        if ($search['value'] !=''){
            $key = $search['value'];
            $this->db->group_start();
            foreach ($list as $ls) {
                $this->db->or_like($ls['select'], $key);
            }           
            $this->db->group_end();
        }

        $order = $this->input->post('order');
        $column = isset($order[0]['column'])?$order[0]['column']:-1;
        
        if($column >= 0 && $column < count($list)){
            $ord = $list[$column]['as'];
            $by = $order[0]['dir'];
            $this->db->order_by($ord , $by);
        } else {
            $ord    = $sort[0];
            $by     = $sort[1];
            $this->db->order_by($ord , $by);
        }

        $this->db->select("SQL_CALC_FOUND_ROWS ".$list[0]['select'] ,FALSE);
        //dynamic select
        foreach($list as $key => $ls) { 
            $this->db->select($ls['select'].' as '.$ls['as']);
        }
        // ex : $this->db->select('transaksi.name');
        $this->db->from($config['from']);
        if ($join!=null) {
            foreach ($join as $key => $jo) {
                $this->db->join($jo['table'],$jo['cond'],$jo['type']);
            }  
        }
        

        //dynamic filter
        foreach($filter as $key => $fil) {
            $this->db->where($fil['key'],$fil['value'],$fil['escape']=null);
        }
        
        $sql = $this->db->get();
        // #1 main query
        $q = $sql->result_array();
        $this->db->select("FOUND_ROWS() AS total_row");
        
        // #2 row (FOUND_ROWS)
        $row = $this->db->get()->row();

        $sEcho = $this->input->post('draw');

        // #3 count_all
        $getCountAll = $this->db->count_all_results('pelanggan');
        
        $output = array(
            "draw" => intval($sEcho),
            "recordsTotal" => $getCountAll,
            "recordsFiltered" => $row->total_row,
            "data" => array()
        );

        $ret['out'] = $output;
        $ret['q']   = $q;

        return $ret;
    }

}

/*!
 * Start Bootstrap - SB Admin 2 v3.3.7+1 (http://startbootstrap.com/template-overviews/sb-admin-2)
 * Copyright 2013-2016 Start Bootstrap
 * Licensed under MIT (https://github.com/BlackrockDigital/startbootstrap/blob/gh-pages/LICENSE)
 */
$(function() {
    $('#side-menu').metisMenu();
});

//Loads the correct sidebar on window load,
//collapses the sidebar on window resize.
// Sets the min-height of #page-wrapper to window size
$(function() {
    $(window).bind("load resize", function() {
        var topOffset = 50;
        var width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
        if (width < 768) {
            $('div.navbar-collapse').addClass('collapse');
            topOffset = 100; // 2-row-menu
        } else {
            $('div.navbar-collapse').removeClass('collapse');
        }

        var height = ((this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height) - 1;
        height = height - topOffset;
        if (height < 1) height = 1;
        if (height > topOffset) {
            $("#page-wrapper").css("min-height", (height) + "px");
        }
    });

    var url = window.location;
    // var element = $('ul.nav a').filter(function() {
    //     return this.href == url;
    // }).addClass('active').parent().parent().addClass('in').parent();
    var element = $('ul.nav a').filter(function() {
        return this.href == url;
    }).addClass('active').parent();

    while (true) {
        if (element.is('li')) {
            element = element.parent().addClass('in').parent();
        } else {
            break;
        }
    }
});

    if($('.datatable').length>0){

        $(".datatable").on("click",'.btn-edit', function(){
            // trigger datatable
            var linkEdit = $(this).attr('data-href');
            $.ajax({
                url: BASE_URL+linkEdit,
                type: 'POST',
                success: function(result){
                    $(".datatable").DataTable().ajax.reload();    
                }});
        });
        $.each($('.datatable'), function(){
            var dtUrl   = $(this).attr('data-url');
            var dtType  = $(this).attr('data-type');
            $(this).dataTable({
                "columnDefs": [{
                        "targets": 'no-sort',
                        "orderable": false
                    }],
                "language"      : {
                    "paginate"  : {
                        next: '>',
                        previous: '<',
                    }
                },
                "processing"    : true,
                'serverSide'    : true,
                'info'          : false,
                'scrollY'       : '100%',
                // 'sScrollX' : '100%',
                'aaSorting'  :[],
                'ajax' : {
                    'url' : BASE_URL+"/"+dtUrl,
                    'type': 'POST',
                    'data':{'getDatatable': 'getDatatable','type':dtType}
                    }
            })
        });
    }

    if($('.datatable-offline').length>0){
            $('.datatable-offline').dataTable({
                "columnDefs": [{
                        "targets": 'no-sort',
                        "orderable": false
                    }],
                "language"      : {
                    "paginate"  : {
                        next: '>',
                        previous: '<',
                    }
                },
                "processing"    : true,
                // 'serverSide'    : true,
                'info'          : false,
                'scrollY'       : '100%',
                // 'sScrollX' : '100%',
                'aaSorting'  :[],
                'ajax' : {
                    'url' : 'http://listrik.local/offline/datatable/load',
                    // 'type': 'POST',
                    // 'data':{'getDatatable': 'getDatatable','type':dtType}
                    }
            })
    }


    $('#tagihan-now-refresh').click(function(){
        //trigger tagihan keterangan
        var url = $(this).attr('data-url');
        $.get(url, function(data, status){
            $('#tagihan-now-total').text(data.total);
            $('#tagihan-now-terkumpul').text(data.terkumpul);
            $('#tagihan-now-kurang-bayar').text(data.kurang_bayar);
        });
    })
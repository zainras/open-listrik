var months = ['Jan','Feb','Mar','Apr','Mei','jun','Jul','Jul'];

$(function() {
    var areaChart = [];
    $.ajax({
        url : 'http://listrik.local/api/main/tagihan-morris.json',
        type : 'GET',
        success : function(data){
            chart(data);
        },
    });
    Morris.Donut({
        element: 'morris-donut-chart',
        data: [{
            label: "Download Sales",
            value: 12
        }, {
            label: "In-Store Sales",
            value: 30
        }, {
            label: "Mail-Order Sales",
            value: 20
        }],
        resize: true
    });

    // Morris.Bar({
    //     element: 'morris-bar-chart',
    //     data: [{
    //         y: '2006',
    //         a: 100,
    //         b: 90
    //     }, {
    //         y: '2007',
    //         a: 75,
    //         b: 65
    //     }, {
    //         y: '2008',
    //         a: 50,
    //         b: 40
    //     }, {
    //         y: '2009',
    //         a: 75,
    //         b: 65
    //     }, {
    //         y: '2010',
    //         a: 50,
    //         b: 40
    //     }, {
    //         y: '2011',
    //         a: 75,
    //         b: 65
    //     }, {
    //         y: '2012',
    //         a: 100,
    //         b: 90
    //     }],
    //     xkey: 'y',
    //     ykeys: ['a', 'b'],
    //     labels: ['Series A', 'Series B'],
    //     hideHover: 'auto',
    //     resize: true
    // });
    
});


    function tagihanChart(data) {
        Morris.Area({
            element: 'morris-area-chart',
            data: data,
            xkey: 'periode',
            ykeys: ['nominal'],
            labels: ['nominal','periode'],
            xLabelFormat : function (x) {
                console.log(x);
                // var month = moment(x,'YYYY-MM-DD','id').format('MMMM');
                // return month;
            },
            pointSize: 2,
            hideHover: 'auto',
            resize: true,
            // dateFormat : function(x) {
            //     var month = moment(x,'YYYY-MM-DD','id').format('MMMM');
            //     return month;
            // }
        });
    }

    function chart(data) {
    //--------------
    //- AREA CHART -
    //--------------
    //
    // Get context with jQuery - using jQuery's .get() method.
    // var areaChartCanvas = $("#areaChart");
    var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var areaChart = new Chart(areaChartCanvas);
    console.log(data);
    var areaChartData = {
      labels: myMonth(data),
      datasets: [
        {
          label: "Digital Goods",
          fillColor: "rgba(60,141,188,0.9)",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: myNominal(data),
        }
      ]
    };

    var areaChartOptions = {
      scaleLabel:  function(label){
        return 'Rp.'+label.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g,".");
      },
      //Boolean - If we should show the scale at all
      showScale: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: false,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - Whether the line is curved between points
      bezierCurve: true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension: 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot: false,
      //Number - Radius of each point dot in pixels
      pointDotRadius: 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth: 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius: 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke: true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth: 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true
    };

    //Create the line chart
    areaChart.Line(areaChartData, areaChartOptions);
}

function myMonth(data)
{
    var month = [];
    $.each(data,function(index,value){
        monthName = moment(value.periode,'MM-YYYY','id').format('MMMM YYYY');
        month.push(monthName);
    });
    return month;
}

function myNominal(data)
{
    var ret = [];
    $.each(data,function(index,value){
        ret.push(value.nominal);
    });
    console.log(ret);
    return ret;
}